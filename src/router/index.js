// 定义router对象  导出给main.js使用
import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from '@/views/login/login.vue'
import Home from '@/views/home'
import Welcome from '@/views/welcome'
import Article from '@/views/article'
import Noutnd from '@/views/404'
import Image from '@/views/Image'
import Publish from '@/views/publish'
import comMent from '@/views/comment'
import Seeting from '@/views/seeting'
import Fans from '@/views/fans'

import store from '@/store'
Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  // 路由规则配置
  // name的作用给当前路由规则取名  $router.push('/login')  或者 $router.push({name:'login'})
  routes: [
    { path: '/login', name: 'login', component: Login },
    // {
    //   path: '/',
    //   component: Home,
    //   children: [
    //     { path: '/', name: 'welcome', component: Welcome }
    //   ]
    // }
    {
      path: '/',
      component: Home,
      redirect: '/welcome',
      children: [
        { path: '/welcome', name: 'welcome', component: Welcome },
        { path: '/article', name: 'article', component: Article },
        { path: '/image', name: 'image', component: Image },
        { path: '/publish', name: 'publish', component: Publish },
        { path: '/comment', name: 'comment', component: comMent },
        { path: '/seeting', name: 'seeting', component: Seeting },
        { path: '/fans', name: 'fans', component: Fans }

      ]
    },
    { path: '*', name: '404', component: Noutnd }
  ]
})

router.beforeEach((to, from, next) => {
  // 判断是不是登录路由
  // if(to.path === '/login') return next()

  // if(!store.getUser().token) return next('/login')

  // next()

  if (to.path !== '/login' && !store.getUser().token) return next('/login')
  next()
})

export default router
