import axios from 'axios'

import store from '@/store'

import router from '@/router'

import JSONBig from 'json-bigint'
// 转成能够支持最大安全数值的数据类型。
axios.defaults.transformResponse = [
  (data) => {
    try {
      return JSONBig.parse(data)
    } catch (e) {
      return data
    }
  }
]
axios.defaults.baseURL = 'http://ttapi.research.itcast.cn/mp/v1_0/'
axios.interceptors.request.use((config) => {
  config.headers = {
    Authorization: `Bearer ${store.getUser().token}`
  }
  return config
}, (error) => {
  // 错误处理
  return Promise.reject(error)
})

// 添加响应拦截
axios.interceptors.response.use((res) => {
  return res
}, (error) => {
  if (error.response.status === 401) {
    router.push('/login')
  }
})
export default axios
